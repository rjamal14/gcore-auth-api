class ApplicationController < ActionController::Base
    # Devise code
    before_action :configure_permitted_parameters, if: :devise_controller?
    respond_to :json

    I18n.load_path << Dir[File.expand_path('config/locales') + '/*.yml']

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    end

    def current_resource_owner
        User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
    end
end
