# ProfileController
module Api
  module V1
    # ProfileController
    class ProfileController < ApplicationController
      before_action :doorkeeper_authorize!
      def me
        render json: current_resource_owner
      end
    end
  end
end
